package entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    String name;
    String asin;
    BigDecimal clickShare;
    BigDecimal conversionShare;
}
