public class Util {

    public static String GET_ENTRIES_QUERY =
            "SELECT   e.entry_date as \"date\", \n" +
            "         e.ranking as \"ranking\",\n" +
            "         p1.product_name as \"productName1\",\n" +
            "         p1.product_asin as \"productAsin1\",\n" +
            "         d.click_share_1 as \"productClickShare1\",\n" +
            "         d.conv_share_1 as \"productConvShare1\",\n" +
            "         p2.product_name as \"productName2\",\n" +
            "         p2.product_asin as \"productAsin2\",\n" +
            "         d.click_share_2 as \"productClickShare2\",\n" +
            "         d.conv_share_2 as \"productConvShare2\",\n" +
            "         p3.product_name as \"productName3\",\n" +
            "         p3.product_asin as \"productAsin3\",\n" +
            "         d.click_share_3 as \"productClickShare3\",\n" +
            "         d.conv_share_3 as \"productConvShare3\"\n" +
            "FROM     amzvc_data.entries e\n" +
            "    JOIN amzvc_data.keywords k on e.keyword_id = k.id\n" +
            "    JOIN amzvc_data.entry_data d on e.entry_data_id = d.id\n" +
            "    JOIN amzvc_data.products p1 on d.product_id_1 = p1.id\n" +
            "    JOIN amzvc_data.products p2 on d.product_id_2 = p2.id\n" +
            "    JOIN amzvc_data.products p3 on d.product_id_3 = p3.id\n" +
            "WHERE   e.entry_date >= ?::date \n" +
            "    AND e.entry_date <= ?::date\n" +
            "    AND e.keyword_id = (SELECT id FROM amzvc_data.keywords WHERE keyword_name = ?)\n" +
            "ORDER BY e.entry_date";

}
