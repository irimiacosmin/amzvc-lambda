import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import dto.DailyEntryDTO;
import entity.Product;
import rest.RequestDetails;
import rest.ResponseDetails;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AmzvcRequestHandler implements RequestHandler<RequestDetails, ResponseDetails> {

    public ResponseDetails handleRequest(RequestDetails requestDetails, Context context) {
        //RequestDetails requestDetails = new RequestDetails("airpods", "2019-04-10", "2019-06-30");


        return getEntries(requestDetails);
    }

    private Connection getConnection() throws SQLException {
        String url = "jdbc:postgresql://amzvc-db.cc3pywsgzfdz.eu-west-2.rds.amazonaws.com:5432/vladhammer";
        Properties props = new Properties();
        props.setProperty("user","vladhammer");
        props.setProperty("password","amzvc2019");
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
        }
        return DriverManager.getConnection(url, props);
    }


    private ResponseDetails getEntries(RequestDetails requestDetails) {
        try{
            List<DailyEntryDTO> dailyEntryDTOList = new ArrayList<>();
            Connection connection = getConnection();

            PreparedStatement updateemp = connection.prepareStatement(Util.GET_ENTRIES_QUERY);
            updateemp.setString(1,requestDetails.getStartDate());
            updateemp.setString(2,requestDetails.getEndDate());
            updateemp.setString(3,requestDetails.getKeyword());

            ResultSet rs = updateemp.executeQuery();
            while (rs.next()) {
                List<Product> productList = new ArrayList<>();

                Date entryDate = rs.getDate("date");
                Integer ranking = rs.getInt("ranking");

                String productName1 = rs.getString("productName1");
                String productAsin1 = rs.getString("productAsin1");
                BigDecimal productClickShare1 = rs.getBigDecimal("productClickShare1");
                BigDecimal productConvShare1 = rs.getBigDecimal("productConvShare1");

                String productName2 = rs.getString("productName2");
                String productAsin2 = rs.getString("productAsin2");
                BigDecimal productClickShare2 = rs.getBigDecimal("productClickShare2");
                BigDecimal productConvShare2 = rs.getBigDecimal("productConvShare2");

                String productName3 = rs.getString("productName3");
                String productAsin3 = rs.getString("productAsin3");
                BigDecimal productClickShare3 = rs.getBigDecimal("productClickShare3");
                BigDecimal productConvShare3 = rs.getBigDecimal("productConvShare3");

                Product product1 = new Product(productName1, productAsin1, productClickShare1, productConvShare1);
                Product product2 = new Product(productName2, productAsin2, productClickShare2, productConvShare2);
                Product product3 = new Product(productName3, productAsin3, productClickShare3, productConvShare3);
                productList.add(product1);
                productList.add(product2);
                productList.add(product3);
                dailyEntryDTOList.add(new DailyEntryDTO(entryDate, ranking, productList));
            }
            return new ResponseDetails(dailyEntryDTOList);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
