package dto;

import entity.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DailyEntryDTO {

    Date entryDate;
    Integer ranking;
    List<Product> products;
}
